open Typerep_lib.Std
open SCaml

(* ppx_typerep_conv produces code like `include X` inappropriate for scamlc.
   [@@deriving typerep] must be in a module without [@@@SCaml].
*)

module Token_metadata = struct
  (* TZIP-012 and TZIP-021 *)
  type info = (string, bytes) Map.t [@@deriving typerep]

  type entry =
    { token_id   : nat
    ; token_info : info
    } [@@deriving typerep]

  (* According to TZIP-012 specification, this nat is equal to token_id.
     This is because the key is hashed.  Token_ids in the value makes it possible
     to get them by loading the big_map image via RPC.
  *)
  type t = (nat, entry) BigMap.t [@@deriving typerep]
end

module Contract_metadata = struct
  (* TZIP-016 *)
  type t = (string, bytes) BigMap.t [@@deriving typerep]
end

type operator =
  { owner    : address;
    operator : address;
    token_id : nat
  } [@@deriving typerep]

type storage =
  { ledger         : ((address * nat), nat) big_map (* multi asset contract *)
  ; operators      : operator Set.t
  ; token_metadata : Token_metadata.t
  ; metadata       : Contract_metadata.t
  ; administrator  : address; (* w/ super power *)
  } [@@deriving typerep]

type tx =
  { to_      : address
  ; token_id : nat
  ; amount   : nat
  } [@@deriving typerep]

type transfer =
  { from_ : address
  ; txs   : tx list
  }  [@@deriving typerep]

type mint_request =
  { owner      : address
  ; token_id   : nat
  ; token_info : (string, bytes) Map.t
  ; amount     : nat
  } [@@deriving typerep]

type update_operator_param =
  | Add_operator    of operator
  | Remove_operator of operator
  [@@deriving typerep]

type burn_request =
  { token_id : nat
  ; amount   : nat
  } [@@deriving typerep]

type balance_request =
  { owner    : address
  ; token_id : nat
  } [@@deriving typerep]

type balance_response =
  { request : balance_request
  ; balance : nat
  } [@@deriving typerep]

type balance_of_param =
  { requests : balance_request list
  ; callback : balance_response list contract
  }
  (* We cannot have typerep for contract.
     It is ok since we have no literal values for it.
  *)
