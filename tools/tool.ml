open Typerep_lib.Std
(* open Meta_conv.Open *)
open Json_conv.Default

module Micheline = SCaml_michelson.Micheline
module Michelson = SCaml_michelson.Michelson

let sort_tezos_map = List.sort (fun (k1, _) (k2, _) -> Stdlib.compare k1 k2)

module Json = struct
  include Tiny_json.Json
  let t_of_json ?trace:_ x = Ok x
  let json_of_t x = x

  let from_file fn =
    let ic = open_in fn in
    try parse_ch ic with
    | e -> close_in ic; raise e

  let rec format_list sep f ppf = function
    | [] -> ()
    | [x] -> f ppf x
    | x::xs -> f ppf x; Format.fprintf ppf sep; format_list sep f ppf xs

  let rec format_compact ppf =
    let open Format in
    function
    | String s ->
        let buf = Buffer.create (String.length s * 2) in
        Buffer.add_char buf '"';
        for i = 0 to String.length s - 1 do
          let c = String.unsafe_get s i in
          match c with
          | '"' -> Buffer.add_string buf "\\\""
          | '\\' -> Buffer.add_string buf "\\\\"
          | '\b' -> Buffer.add_string buf "\\b"
          | '\012' -> Buffer.add_string buf "\\f"
          | '\n' -> Buffer.add_string buf "\\n"
          | '\r' -> Buffer.add_string buf "\\r"
          | '\t' -> Buffer.add_string buf "\\t"
          | _ when Char.code c <= 32 && c <> ' ' ->
              Printf.ksprintf (Buffer.add_string buf) "\\u%04X" (Char.code c)
          | _ -> Buffer.add_char buf c
        done;
        Buffer.add_char buf '"';
        pp_print_string ppf (Buffer.contents buf)
    | Number s -> fprintf ppf "%s" s
    | Object o ->
        fprintf ppf "{%a}"
          (format_list "," (fun ppf (s,v) -> fprintf ppf "\"%s\":%a" s format_compact v)) o
    | Array ts -> fprintf ppf "[%a]" (format_list "," format_compact) ts
    | Bool b -> fprintf ppf "%b" b
    | Null -> fprintf ppf "null"

  let to_string = Format.asprintf "%a" format_compact
end

type init_storage =
  { administrator : string
  ; metadata : Json.t (* XXX required fields? *)
  } [@@deriving conv{json}]

let save typerep fn x =
  Micheline.save
    Michelson.Constant.pp
    fn
    (SCamlc.Typerep.to_michelson typerep x)

let pp_set ppf =
  Format.pp_set_margin ppf 199999 ;
  Format.pp_set_max_indent ppf 99999 ;
  Format.pp_set_max_boxes ppf 99999

let print typerep x =
  pp_set Format.std_formatter;
  Format.printf "%a@."
    Michelson.Constant.pp
      (SCamlc.Typerep.to_michelson typerep x)

let write typerep fn x =
  Micheline.save
    Michelson.Constant.pp
    fn
    (SCamlc.Typerep.to_michelson typerep x)

let init_storage fn =
  let t = Json.from_file fn in
  match init_storage_of_json t with
  | Error e ->
      Format.eprintf "Error: %a@." (Meta_conv.Error.format Json.format) e;
      exit 1
  | Ok init_storage ->
      let v =
        let open SCaml in
        let open Nfttypes in
        { ledger = BigMap [];
          operators = Set [];
          token_metadata = BigMap [];
          metadata =
            BigMap [ "", Bytes.of_string "tezos-storage:metadata";
                     "metadata",
                     let s = Json.to_string init_storage.metadata in
                     Bytes.of_string s ];
          administrator = Address init_storage.administrator
        }
      in
      print Nfttypes.typerep_of_storage v

type mint_entry =
  { owner : string;
    token_id : int;
    token_info : Json.t (* XXX required fields? *);
    amount : int
  } [@@deriving conv{json}]

type mint = mint_entry list [@@deriving conv{json}]

let mint fn =
  let t = Json.from_file fn in
  match mint_of_json t with
  | Error e ->
      Format.eprintf "Error: %a@." (Meta_conv.Error.format Json.format) e;
      exit 1
  | Ok mint ->
      let v =
        List.map (fun { owner; token_id; token_info; amount } ->
            let open SCaml in
            let open Nfttypes in
            { owner = Address owner;
              token_id = Nat token_id;
              token_info =
                Map (sort_tezos_map
                     @@ List.map (fun (k,v) -> (k, Bytes.of_string @@
                                                match v with
                                                | Json.String s -> s
                                                | Number s -> s
                                                | _ -> Json.to_string v))
                     @@ Json.as_object token_info);
              amount = Nat amount }) mint
      in
      print (typerep_of_list Nfttypes.typerep_of_mint_request) v

type update_token_info_entry =
  { token_id : int;
    token_info : Json.t (* XXX required fields? *);
  } [@@deriving conv{json}]

type update_token_info = update_token_info_entry list [@@deriving conv{json}]

let update_token_metadata fn =
  let t = Json.from_file fn in
  match update_token_info_of_json t with
  | Error e ->
      Format.eprintf "Error: %a@." (Meta_conv.Error.format Json.format) e;
      exit 1
  | Ok requests ->
      let v =
        List.map (fun ({ token_id; token_info } : update_token_info_entry) ->
            let open SCaml in
            let open Nfttypes in
            let open Token_metadata in
            { token_id = Nat token_id;
              token_info =
                Map (sort_tezos_map
                     @@ List.map (fun (k,v) -> (k, Bytes.of_string @@
                                                match v with
                                                | Json.String s -> s
                                                | Number s -> s
                                                | _ -> Json.to_string v))
                     @@ Json.as_object token_info); }) requests
      in
      print (typerep_of_list Nfttypes.Token_metadata.typerep_of_entry) v

let () =
  match Sys.argv.(1) with
  | "init" -> init_storage Sys.argv.(2)
  | "mint" -> mint Sys.argv.(2)
  | "update_token_metadata" -> update_token_metadata Sys.argv.(2)
  | _ -> assert false

(*
let () =
  save (typerep_of_list Nfttypes.typerep_of_mint_request) "mint_mock.tz"
  [ { owner = admin_mock;
      token_id = Nat 0;
      token_info = Map (Stdlib.List.sort
                          (fun (k1, _) (k2, _) -> Stdlib.compare k1 k2) [ "name", Bytes.of_string "nam";
                         "description", Bytes.of_string "desc";
                         "tags", Bytes.of_string "[\"tag\"]";
                         "symbol", Bytes.of_string "MYNFT";
                         "artifactUri", Bytes.of_string "https://dailambda.jp/strass/nft/triangle.png";
                         "displayUri", Bytes.of_string "https://dailambda.jp/strass/nft/triangle.png";
                         "thumbnailUri", Bytes.of_string "https://dailambda.jp/strass/nft/triangle-thumb.png";
                         "minter", Bytes.of_string "tz1VQS822Hwf512CmGKtjVdm7njtRVX7pHko";
                         "creators", Bytes.of_string "[\"tz1VQS822Hwf512CmGKtjVdm7njtRVX7pHko\"]";
                         "formats", Bytes.of_string "[{\"uri\":\"https://dailambda.jp/strass/nft/triangle.png\",\"mimeType\":\"image/png\"}]";
                         "decimals", Bytes.of_string "0";
                         "isBooleanAmount", Bytes.of_string "true";
                         "shouldPreferSymbol", Bytes.of_string "false"
                       ]);
      amount = Nat 10
       } ]

let () =
  save (typerep_of_list Nfttypes.typerep_of_mint_request) "mint_hyblid_mock.tz"
    [ { owner = admin_mock;
        token_id = Nat 1;
        token_info = Map [ "", Bytes.of_string "ipfs://QmS6WeGhrk3qdCj3VtYKFs2PxtPitRz9sXWu7NZMovCX2c";
                           "color", Bytes "0x0000ff" ];
        amount = Nat 10
        } ]

let () =
  save (typerep_of_list Nfttypes.typerep_of_transfer) "transfer_mock.tz"
  [ { from_ = admin_mock;
      txs = [ { to_ = Address "tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN";
                token_id = Nat 0;
                amount = Nat 1 } ] } ]

let () =
  save
    (typerep_of_list Nfttypes.typerep_of_update_operator_param)
    "update_operators_mock.tz"
    [ Add_operator { owner = Address "tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN";
                     operator = admin_mock;
                     token_id = Nat 0 } ]

let () =
  save (typerep_of_list Nfttypes.typerep_of_transfer) "transfer2_mock.tz"
    [ { from_ = Address "tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN";
        txs = [ { to_ = admin_mock;
                  token_id = Nat 0;
                  amount = Nat 1 } ] } ]

let admin_test = Address "tz1VQS822Hwf512CmGKtjVdm7njtRVX7pHko"

let () =
  save Nfttypes.typerep_of_storage "init_storage_test.tz"
    { ledger = BigMap [];
      operators = Set [];
      token_metadata = BigMap [];
      metadata =
        BigMap [ "", Bytes.of_string "tezos-storage:metadata";
                 "metadata", Bytes.of_string {|{"authors":["foo@bar.jp"],"name":"my nft","description":"my nft","interfaces":["TZIP-12"]}|} ];
      administrator = admin_test
    }

let () =
  save (typerep_of_list Nfttypes.typerep_of_mint_request) "mint_test.tz"
    [ { owner = admin_test;
        token_id = Nat 0;
        token_info = Map (Stdlib.List.sort
                            (fun (k1, _) (k2, _) -> Stdlib.compare k1 k2) [ "name", Bytes.of_string "nam";
                           "description", Bytes.of_string "desc";
                           "tags", Bytes.of_string "[\"tag\"]";
                           "symbol", Bytes.of_string "MYNFT";
                           "artifactUri", Bytes.of_string "https://dailambda.jp/strass/nft/triangle.png";
                           "displayUri", Bytes.of_string "https://dailambda.jp/strass/nft/triangle.png";
                           "thumbnailUri", Bytes.of_string "https://dailambda.jp/strass/nft/triangle-thumb.png";
                           "minter", Bytes.of_string "tz1VQS822Hwf512CmGKtjVdm7njtRVX7pHko";
                           "creators", Bytes.of_string "[\"tz1VQS822Hwf512CmGKtjVdm7njtRVX7pHko\"]";
                           "formats", Bytes.of_string "[{\"uri\":\"https://dailambda.jp/strass/nft/triangle.png\",\"mimeType\":\"image/png\"}]";
                           "decimals", Bytes.of_string "0";
                           "isBooleanAmount", Bytes.of_string "true";
                           "shouldPreferSymbol", Bytes.of_string "false"
                         ]);
        amount = Nat 10
        } ]

let () =
  save (typerep_of_list Nfttypes.typerep_of_mint_request) "mint_hyblid_test.tz"
    [ { owner = admin_test;
        token_id = Nat 1;
        token_info = Map [ "", Bytes.of_string "ipfs://QmS6WeGhrk3qdCj3VtYKFs2PxtPitRz9sXWu7NZMovCX2c";
                           "color", Bytes "0x0000ff" ];
        amount = Nat 10 } ]

let () =
  save (typerep_of_list Nfttypes.typerep_of_mint_request)
    "mint_json_test.tz"
    [ { owner = admin_test;
        token_id = Nat 2;
        token_info = Map [ "", Bytes.of_string "https://dailambda.jp/strass/nft/token_metadata.json"; ];
        amount = Nat 10; } ]

let () =
  save (typerep_of_list Nfttypes.typerep_of_mint_request) "mint_json_part_test.tz"
  [ { owner = admin_test;
      token_id = Nat 3;
      token_info = Map [ "", Bytes.of_string "https://dailambda.jp/strass/nft/token_metadata_no_name.json";
                         "name", Bytes.of_string "name in chain"
                       ];
      amount = Nat 10;
       } ]

let () =
  save (typerep_of_list Nfttypes.typerep_of_transfer) "transfer_test.tz"
    [ { from_ = admin_test;
        txs = [ { to_ = Address "tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN";
                  token_id = Nat 0;
                  amount = Nat 1 } ] } ]

let () =
  save (typerep_of_list Nfttypes.typerep_of_update_operator_param) "update_operators_test.tz"
  [ Add_operator { owner = Address "tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN";
                   operator = admin_test;
                   token_id = Nat 0 } ]

let () =
  save (typerep_of_list Nfttypes.typerep_of_transfer) "transfer2_test.tz"
    [ { from_ = Address "tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN";
        txs = [ { to_ = admin_test;
                  token_id = Nat 0;
                  amount = Nat 1 } ] } ]

let () =
  save (typerep_of_list Nfttypes.typerep_of_mint_request) "mint4_test.tz"
    [ { owner = admin_test;
        token_id = Nat 4;
        token_info = Map (Stdlib.List.sort
                            (fun (k1, _) (k2, _) -> Stdlib.compare k1 k2) [ "name", Bytes.of_string "tkn4";
                           "description", Bytes.of_string "desc";
                           "symbol", Bytes.of_string "MYNFT";
                           "artifactUri", Bytes.of_string "https://dailambda.jp/strass/nft/triangle.png";
                           "displayUri", Bytes.of_string "https://dailambda.jp/strass/nft/triangle.png";
                           "thumbnailUri", Bytes.of_string "https://dailambda.jp/strass/nft/triangle-thumb.png";
                           "decimals", Bytes.of_string "0";
                           "isBooleanAmount", Bytes.of_string "true";
                           "shouldPreferSymbol", Bytes.of_string "false"
                         ]);
        amount = Nat 10; } ]
*)
