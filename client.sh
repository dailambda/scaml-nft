#!/bin/sh

set -e

export TEZOS_CLIENT_DIR=$HOME/.tezos-test-client

function client
{
    ~/.share/tezos-mainnet/tezos-client -E https://hangzhounet.smartpy.io "$@"
}

# client activate account faucet with ~/Downloads/hangzhounet.json 
# client get balance for faucet

BUILDDIR=../../../_build/default/ppx/tests/nft

# client originate contract nft0 transferring 0 from faucet running ../../../_build/default/ppx/tests/nft/nft.tz --init "`cat $BUILDDIR/init_storage_test.tz`" --burn-cap 1 -f
# client transfer 0 from faucet to nft0 --entrypoint mint --arg "`cat $BUILDDIR/mint_test.tz`" --burn-cap 0.191
# client transfer 0 from faucet to nft0 --entrypoint mint --arg "`cat $BUILDDIR/mint_hyblid_test.tz`" --burn-cap 0.191

# client transfer 0 from faucet to nft0 --entrypoint mint --arg "`cat $BUILDDIR/mint_json_test.tz`" --burn-cap 0.191
# client transfer 0 from faucet to nft0 --entrypoint mint --arg "`cat $BUILDDIR/mint_json_part_test.tz`" --burn-cap 0.191
# client transfer 0 from faucet to nft0 --entrypoint mint --arg "`cat $BUILDDIR/mint4_test.tz`" --burn-cap 0.191

# client gen keys test1





