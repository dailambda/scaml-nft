[@@@SCaml contract = "nft"]

open Nfttypes
open SCaml

module Error = struct
  exception FA2_TOKEN_UNDEFINED
  (* One of the specified token_ids is not defined within the FA2 contract *)

  exception FA2_INSUFFICIENT_BALANCE
  (* A token owner does not have sufficient balance to transfer tokens from owner's account *)

  exception FA2_TX_DENIED
  (* A transfer failed because of operator_transfer_policy == No_transfer *)

  exception FA2_NOT_OWNER
  (* A transfer failed because operator_transfer_policy == Owner_transfer and it is invoked not by the token owner *)

  exception FA2_NOT_OPERATOR
  (* A transfer failed because operator_transfer_policy == Owner_or_operator_transfer and it is invoked neither by the token owner nor a permitted operator *)

  exception FA2_OPERATORS_UNSUPPORTED
  (* update_operators entrypoint is invoked and operator_transfer_policy is No_transfer or Owner_transfer *)

  exception FA2_RECEIVER_HOOK_FAILED
  (* The receiver hook failed. This error MUST be raised by the hook implementation *)


  exception FA2_SENDER_HOOK_FAILED
  (* The sender failed. This error MUST be raised by the hook implementation *)


  exception FA2_RECEIVER_HOOK_UNDEFINED
  (* Receiver hook is required by the permission behavior, but is not implemented by a receiver contract *)
end

open Error

let [@entry] transfer transfers ({ ledger; operators; _ } as storage) =
  let sender = Global.get_sender () in
  let ledger =
    List.fold_left (fun ledger { from_; txs } ->
        List.fold_left (fun ledger { to_; token_id; amount } ->
            if sender <> from_
               && not (Set.mem { operator= sender; owner= from_; token_id }
                         operators)
            then raise FA2_NOT_OPERATOR;

            match amount with
            | Nat 0 -> ledger
            | _ ->
                let from_balance =
                  let x = BigMap.find_with_default (from_, token_id) ledger (Nat 0) in
                  let x = x -^ amount in
                  if x < Int 0 then raise FA2_INSUFFICIENT_BALANCE;
                  if x = Int 0 then None else Some (abs x)
                in
                let to_balance =
                  Some (
                    match BigMap.find (to_, token_id) ledger with
                    | None -> amount
                    | Some n -> n +^ amount
                  )
                in
                BigMap.update (from_, token_id) from_balance
                @@ BigMap.update (to_, token_id) to_balance ledger)
          ledger txs)
      ledger transfers
  in
  [], { storage with ledger; operators }

let [@entry] balance_of { requests; callback } ({ ledger; _ } as storage) =
  let response =
    List.map (fun ({ owner; token_id } as request) ->
        let balance =
          BigMap.find_with_default (owner,token_id) ledger (Nat 0)
        in
        { request; balance }) requests
  in
  [ Operation.transfer_tokens response (Tz 0.) callback ],
  storage

let [@entry] update_operators commands ({ operators; _ } as storage) =
  (* Only the sender can change its operator *)
  let sender = Global.get_sender () in
  let operators =
    List.fold_left (fun operators -> function
        | Add_operator op ->
            if op.owner <> sender then raise FA2_NOT_OWNER;
            Set.add op operators
        | Remove_operator op ->
            if op.owner <> sender then raise FA2_NOT_OWNER;
            Set.remove op operators)
      operators commands
  in
  [], { storage with operators }

exception ADMIN_PERMISSION_DENIED

let is_admin storage =
  let sender = Global.get_sender () in
  sender = storage.administrator

let check_admin storage =
  if not (is_admin storage) then raise ADMIN_PERMISSION_DENIED

(* This replaces all the big_maps inside the storage by new ones with different big_map IDs *)
let [@entry] reset storage _storage =
  check_admin storage;
  [], storage

exception TOKENID_ALREADY_EXISTS

(* Only the admin can mint tokens.  You cannot mint existing token *)
let [@entry] mint mint_requests ({ ledger; token_metadata; _ } as storage) =
  check_admin storage;
  let ledger, token_metadata =
    List.fold_left' (fun ((ledger, token_metadata),
                          { owner; token_id; token_info; amount }) ->
        match BigMap.find (owner, token_id) ledger with
        | Some _ -> raise TOKENID_ALREADY_EXISTS
        | None ->
            (BigMap.add (owner, token_id) amount ledger,
             (* If token_id exists in the token_metadata,
                (it should not exist but), it is overwritten. *)
             BigMap.add token_id Token_metadata.{ token_id; token_info } token_metadata)
      ) (ledger, token_metadata) mint_requests
  in
  [], { storage with ledger; token_metadata }

(* Only the admin can update token_info *)
(* XXXX update_token_metadta *)
let [@entry] update_token_info requests ({ token_metadata; _ } as storage) =
  check_admin storage;
  let token_metadata =
    List.fold_left' (fun (token_metadata, ({ token_id; _} as entry : Token_metadata.entry)) ->
        match BigMap.find token_id token_metadata with
        | None -> raise FA2_TOKEN_UNDEFINED
        | Some _ -> BigMap.add token_id entry token_metadata)
      token_metadata requests
  in
  [], { storage with token_metadata }

(* Only the owner can burn its tokens *)
let [@entry] burn ({ token_id; amount } : burn_request) ({ ledger; _ } as storage) =
  let sender = Global.get_sender () in
  let balance =
    let x = BigMap.find_with_default (sender, token_id) ledger (Nat 0) in
    let x = x -^ amount in
    if x < Int 0 then raise FA2_INSUFFICIENT_BALANCE;
    if x = Int 0 then None else Some (abs x)
  in
  let ledger = BigMap.update (sender, token_id) balance ledger in
  [], { storage with ledger }

let [@entry] set_admin administrator storage =
  check_admin storage;
  [], { storage with administrator }

let [@view] get_balance ({owner; token_id} : balance_request) { ledger; _ } =
  BigMap.find_with_default (owner, token_id) ledger (Nat 0)

let [@view] token_metadata_opt token_id { token_metadata; _ } =
  BigMap.find token_id token_metadata
