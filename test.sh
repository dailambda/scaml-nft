set -e

BUILDDIR=../../../_build/default/ppx/tests/nft

bootstrap1=tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx

TEZOS_CLIENT=`which tezos-client || true`
if [ ! -n "$TEZOS_CLIENT" ]; then
    echo need tezos client
    exit 1
fi
TEZOS_CLIENT="$TEZOS_CLIENT --protocol PtHangz2aRng --mode mockup --base-dir _mock"

dune build

# Prepare mock environment
rm -rf _mock
mkdir _mock
$TEZOS_CLIENT create mockup --protocol-constants ../../../tests/protocol-constants.json
$TEZOS_CLIENT config init

# Deployment
$TEZOS_CLIENT originate contract nft transferring 0 from bootstrap1 running $BUILDDIR/nft.tz --init "`cat $BUILDDIR/init_storage.tz`" --burn-cap 1
$TEZOS_CLIENT get contract storage for nft
$TEZOS_CLIENT hash data '""' of type string # > expru5X1yxJG6ezR2uHMotwMLNmSzQyh5t1vUnhjx4cS6Pv9qE1Sdo
$TEZOS_CLIENT get element expru5X1yxJG6ezR2uHMotwMLNmSzQyh5t1vUnhjx4cS6Pv9qE1Sdo of big map 6
echo SHOULD BE "...metadata"

# Reset
$TEZOS_CLIENT transfer 0 from bootstrap1 to nft --entrypoint reset --arg "`cat $BUILDDIR/init_storage_mock.tz`"
$TEZOS_CLIENT get contract storage for nft
$TEZOS_CLIENT get element expru5X1yxJG6ezR2uHMotwMLNmSzQyh5t1vUnhjx4cS6Pv9qE1Sdo of big map 9
echo SHOULD BE "...metadata"

# Mint
$TEZOS_CLIENT transfer 0 from bootstrap1 to nft --entrypoint mint --arg "`cat $BUILDDIR/mint.tz`" --burn-cap 0.191
$TEZOS_CLIENT transfer 0 from bootstrap1 to nft --entrypoint mint --arg "`cat $BUILDDIR/mint-hyblid.tz`" --burn-cap 0.191

# 7 %ledger
# 8 %token_metadta
# 9 %metadata

$TEZOS_CLIENT rpc get /chains/main/blocks/head/context/big_maps/7
$TEZOS_CLIENT rpc get /chains/main/blocks/head/context/big_maps/8
$TEZOS_CLIENT rpc get /chains/main/blocks/head/context/big_maps/9

# raw is broken in mock!
$TEZOS_CLIENT rpc get /chains/main/blocks/head/context/raw/json

# Trasnfer
$TEZOS_CLIENT transfer 0 from bootstrap1 to nft --entrypoint transfer --arg "`cat $BUILDDIR/transfer.tz`"
$TEZOS_CLIENT transfer 0 from bootstrap1 to nft --entrypoint transfer --arg "`cat $BUILDDIR/transfer.tz`" && (echo error; exit 1) || echo failed expectedly

# Update operators
$TEZOS_CLIENT transfer 0 from bootstrap2 to nft --entrypoint update_operators --arg "`cat $BUILDDIR/update_operators.tz`" --burn-cap 0.015
## transfer bootstrap2's token by its operator bootstrap1
$TEZOS_CLIENT transfer 0 from bootstrap1 to nft --entrypoint transfer --arg "`cat $BUILDDIR/transfer2.tz`"
$TEZOS_CLIENT rpc get /chains/main/blocks/head/context/big_maps/7


