open Typerep_lib.Std
open SCaml

module Micheline = SCaml_michelson.Micheline
module Michelson = SCaml_michelson.Michelson

let admin_mock = Address "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"

let save typerep fn x =
  Micheline.save
    Michelson.Constant.pp
    fn
    (SCamlc.Typerep.to_michelson typerep x)

let () =
  save Nfttypes.typerep_of_storage "init_storage_mock.tz"
    { ledger = BigMap [];
      operators = Set [];
      token_metadata = BigMap [];
      metadata =
        BigMap [ "", Bytes.of_string "tezos-storage:metadata";
                 "metadata", Bytes.of_string {|{"authors":["foo@bar.jp"],"name":"my nft","description":"my nft","interfaces":["TZIP-12"]}|} ];
      administrator = admin_mock
    }

let () =
  save (typerep_of_list Nfttypes.typerep_of_mint_request) "mint_mock.tz"
  [ { owner = admin_mock;
      token_id = Nat 0;
      token_info = Map (Stdlib.List.sort
                          (fun (k1, _) (k2, _) -> Stdlib.compare k1 k2) [ "name", Bytes.of_string "nam";
                         "description", Bytes.of_string "desc";
                         "tags", Bytes.of_string "[\"tag\"]";
                         "symbol", Bytes.of_string "MYNFT";
                         "artifactUri", Bytes.of_string "https://dailambda.jp/strass/nft/triangle.png";
                         "displayUri", Bytes.of_string "https://dailambda.jp/strass/nft/triangle.png";
                         "thumbnailUri", Bytes.of_string "https://dailambda.jp/strass/nft/triangle-thumb.png";
                         "minter", Bytes.of_string "tz1VQS822Hwf512CmGKtjVdm7njtRVX7pHko";
                         "creators", Bytes.of_string "[\"tz1VQS822Hwf512CmGKtjVdm7njtRVX7pHko\"]";
                         "formats", Bytes.of_string "[{\"uri\":\"https://dailambda.jp/strass/nft/triangle.png\",\"mimeType\":\"image/png\"}]";
                         "decimals", Bytes.of_string "0";
                         "isBooleanAmount", Bytes.of_string "true";
                         "shouldPreferSymbol", Bytes.of_string "false"
                       ]);
      amount = Nat 10
       } ]

let () =
  save (typerep_of_list Nfttypes.typerep_of_mint_request) "mint_hyblid_mock.tz"
    [ { owner = admin_mock;
        token_id = Nat 1;
        token_info = Map [ "", Bytes.of_string "ipfs://QmS6WeGhrk3qdCj3VtYKFs2PxtPitRz9sXWu7NZMovCX2c";
                           "color", Bytes "0x0000ff" ];
        amount = Nat 10
        } ]

let () =
  save (typerep_of_list Nfttypes.typerep_of_transfer) "transfer_mock.tz"
  [ { from_ = admin_mock;
      txs = [ { to_ = Address "tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN";
                token_id = Nat 0;
                amount = Nat 1 } ] } ]

let () =
  save
    (typerep_of_list Nfttypes.typerep_of_update_operator_param)
    "update_operators_mock.tz"
    [ Add_operator { owner = Address "tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN";
                     operator = admin_mock;
                     token_id = Nat 0 } ]

let () =
  save (typerep_of_list Nfttypes.typerep_of_transfer) "transfer2_mock.tz"
    [ { from_ = Address "tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN";
        txs = [ { to_ = admin_mock;
                  token_id = Nat 0;
                  amount = Nat 1 } ] } ]

let admin_test = Address "tz1VQS822Hwf512CmGKtjVdm7njtRVX7pHko"

let () =
  save Nfttypes.typerep_of_storage "init_storage_test.tz"
    { ledger = BigMap [];
      operators = Set [];
      token_metadata = BigMap [];
      metadata =
        BigMap [ "", Bytes.of_string "tezos-storage:metadata";
                 "metadata", Bytes.of_string {|{"authors":["foo@bar.jp"],"name":"my nft","description":"my nft","interfaces":["TZIP-12"]}|} ];
      administrator = admin_test
    }

let () =
  save (typerep_of_list Nfttypes.typerep_of_mint_request) "mint_test.tz"
    [ { owner = admin_test;
        token_id = Nat 0;
        token_info = Map (Stdlib.List.sort
                            (fun (k1, _) (k2, _) -> Stdlib.compare k1 k2) [ "name", Bytes.of_string "nam";
                           "description", Bytes.of_string "desc";
                           "tags", Bytes.of_string "[\"tag\"]";
                           "symbol", Bytes.of_string "MYNFT";
                           "artifactUri", Bytes.of_string "https://dailambda.jp/strass/nft/triangle.png";
                           "displayUri", Bytes.of_string "https://dailambda.jp/strass/nft/triangle.png";
                           "thumbnailUri", Bytes.of_string "https://dailambda.jp/strass/nft/triangle-thumb.png";
                           "minter", Bytes.of_string "tz1VQS822Hwf512CmGKtjVdm7njtRVX7pHko";
                           "creators", Bytes.of_string "[\"tz1VQS822Hwf512CmGKtjVdm7njtRVX7pHko\"]";
                           "formats", Bytes.of_string "[{\"uri\":\"https://dailambda.jp/strass/nft/triangle.png\",\"mimeType\":\"image/png\"}]";
                           "decimals", Bytes.of_string "0";
                           "isBooleanAmount", Bytes.of_string "true";
                           "shouldPreferSymbol", Bytes.of_string "false"
                         ]);
        amount = Nat 10
        } ]

let () =
  save (typerep_of_list Nfttypes.typerep_of_mint_request) "mint_hyblid_test.tz"
    [ { owner = admin_test;
        token_id = Nat 1;
        token_info = Map [ "", Bytes.of_string "ipfs://QmS6WeGhrk3qdCj3VtYKFs2PxtPitRz9sXWu7NZMovCX2c";
                           "color", Bytes "0x0000ff" ];
        amount = Nat 10 } ]

let () =
  save (typerep_of_list Nfttypes.typerep_of_mint_request)
    "mint_json_test.tz"
    [ { owner = admin_test;
        token_id = Nat 2;
        token_info = Map [ "", Bytes.of_string "https://dailambda.jp/strass/nft/token_metadata.json"; ];
        amount = Nat 10; } ]

let () =
  save (typerep_of_list Nfttypes.typerep_of_mint_request) "mint_json_part_test.tz"
  [ { owner = admin_test;
      token_id = Nat 3;
      token_info = Map [ "", Bytes.of_string "https://dailambda.jp/strass/nft/token_metadata_no_name.json";
                         "name", Bytes.of_string "name in chain"
                       ];
      amount = Nat 10;
       } ]

let () =
  save (typerep_of_list Nfttypes.typerep_of_transfer) "transfer_test.tz"
    [ { from_ = admin_test;
        txs = [ { to_ = Address "tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN";
                  token_id = Nat 0;
                  amount = Nat 1 } ] } ]

let () =
  save (typerep_of_list Nfttypes.typerep_of_update_operator_param) "update_operators_test.tz"
  [ Add_operator { owner = Address "tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN";
                   operator = admin_test;
                   token_id = Nat 0 } ]

let () =
  save (typerep_of_list Nfttypes.typerep_of_transfer) "transfer2_test.tz"
    [ { from_ = Address "tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN";
        txs = [ { to_ = admin_test;
                  token_id = Nat 0;
                  amount = Nat 1 } ] } ]

let () =
  save (typerep_of_list Nfttypes.typerep_of_mint_request) "mint4_test.tz"
    [ { owner = admin_test;
        token_id = Nat 4;
        token_info = Map (Stdlib.List.sort
                            (fun (k1, _) (k2, _) -> Stdlib.compare k1 k2) [ "name", Bytes.of_string "tkn4";
                           "description", Bytes.of_string "desc";
                           "symbol", Bytes.of_string "MYNFT";
                           "artifactUri", Bytes.of_string "https://dailambda.jp/strass/nft/triangle.png";
                           "displayUri", Bytes.of_string "https://dailambda.jp/strass/nft/triangle.png";
                           "thumbnailUri", Bytes.of_string "https://dailambda.jp/strass/nft/triangle-thumb.png";
                           "decimals", Bytes.of_string "0";
                           "isBooleanAmount", Bytes.of_string "true";
                           "shouldPreferSymbol", Bytes.of_string "false"
                         ]);
        amount = Nat 10; } ]
